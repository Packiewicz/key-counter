#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/netlink.h>
#include <net/sock.h>
#include <linux/skbuff.h>
#include <linux/time.h>
#include <linux/string.h>
#include <linux/rtc.h>
#include <linux/timekeeping.h>
#include <linux/interrupt.h>

MODULE_DESCRIPTION("Module counting keyboard interrupts");
MODULE_AUTHOR("maciejp");
MODULE_LICENSE("GPL");

#define KEYBOARD_IRQ_NR 1

static struct sock *nl_sk = NULL;
// extern ktime_t kbd_time;
// extern int kbd_irq_count;
 ktime_t kbd_time;
 int kbd_irq_count;

static void recv_msg(struct sk_buff *skb)
{
  struct nlmsghdr *nlh_in;
  struct nlmsghdr *nlh_out;
  struct sk_buff *skb_out;
  char *input;
  int res;
  char buffer[32];
  int len;
  ktime_t local_time;
  struct rtc_time tm;

  nlh_in = (struct nlmsghdr *)skb->data;
  input = (char *)nlmsg_data(nlh_in);
  printk(KERN_INFO "Netlink received msg payload: %s\n", input);

  if(strcmp(input, "getdate") == 0)
  {
    local_time = kbd_time - (sys_tz.tz_minuteswest * 60);
    tm = rtc_ktime_to_tm(local_time);
    len = snprintf(buffer, sizeof(buffer), "Reset date: %04d-%02d-%02d %02d:%02d:%02d", 
      tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
  }
  else if(strcmp(input, "getirqcount") == 0)
  {
    len = snprintf(buffer, sizeof(buffer), "%d", kbd_irq_count);
  }
  else
  {
    len = snprintf(buffer, sizeof(buffer), "%s", "Unrecognized command");
  }

  skb_out = nlmsg_new(len, 0);
  if (!skb_out)
  {
    printk(KERN_INFO "Failed to allocate new skb\n");
    return;
  }

  nlh_out = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, len, 0);
  NETLINK_CB(skb_out).dst_group = 0;
  strncpy(nlmsg_data(nlh_out), buffer, len);
  
  res = nlmsg_unicast(nl_sk, skb_out, nlh_in->nlmsg_pid);
  if (res < 0)
  {
    printk(KERN_INFO "Error while sending back to user\n");
  }
}

static irq_handler_t kbd_irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{
  (void)irq;
  (void)dev_id;
  (void)regs;

  kbd_irq_count++;
  return (irq_handler_t)IRQ_HANDLED;
}

static int __init kbd_init(void)
{
  int result;
  printk(KERN_INFO "Hello module\n");

  struct netlink_kernel_cfg cfg = {
      .input = recv_msg,
  };

  // init default time
  kbd_time = ktime_get_real();

  nl_sk = netlink_kernel_create(&init_net, 31, &cfg);
  if (!nl_sk)
  {
    printk(KERN_INFO "Error creating socket\n");
    return -10;
  }

  result = request_irq(KEYBOARD_IRQ_NR, (irq_handler_t)kbd_irq_handler, IRQF_SHARED, "kbd module", (void *)kbd_irq_handler);
  if(result < 0)
  {
    printk(KERN_INFO "Error request IRQ\n");
  }

  return 0;
}

static void __exit kbd_exit(void)
{
  printk(KERN_INFO "Goodbye module\n");
  netlink_kernel_release(nl_sk);
  free_irq(KEYBOARD_IRQ_NR, (void *)kbd_irq_handler);
}

module_init(kbd_init);
module_exit(kbd_exit);
