#include <sys/socket.h>
#include <unistd.h>
#include <linux/netlink.h>
#include <linux/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_PAYLOAD 1024
#define NETLINK_USER 31

#define COUNTER_SYSCALL 437
#define RESET_SYSCALL 436

// https://github.com/cirosantilli/linux-kernel-module-cheat/tree/e05eb91addeb30a8486fa0926270a36aafe58fe7#netlink-sockets

#define ZERO(x) memset(&x, 0, sizeof(x))

int main()
{	
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr *nlh;
	struct sockaddr_nl src_addr, dest_addr;
	// char input[MAX_PAYLOAD];
	int res, size;
	struct timespec date;
	long irq_count;

	ZERO(msg);
	ZERO(src_addr);
	ZERO(dest_addr);
	ZERO(iov);
	ZERO(date);

	int sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
	if(sock_fd < 0)
	{
		perror("socket error");
		return EXIT_FAILURE;
	}

	src_addr.nl_family = AF_NETLINK;
	src_addr.nl_pid = getpid();

	res = bind(sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr));
	if(res < 0)
	{
		perror("bind error");
		return EXIT_FAILURE;
	}
	
	dest_addr.nl_family = AF_NETLINK;
	nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));

	memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));	

	nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
	nlh->nlmsg_pid = getpid();
	nlh->nlmsg_flags = 0;
	// strcpy(NLMSG_DATA(nlh), "getirqcount");
	strcpy(NLMSG_DATA(nlh), "getdate");

	iov.iov_base = (void *)nlh;
	iov.iov_len = nlh->nlmsg_len;
	msg.msg_name = (void *)&dest_addr;
	msg.msg_namelen = sizeof(dest_addr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	
	res = sendmsg(sock_fd, &msg, 0);
	if(res > 0)
	{
		printf("Sent msg\n");
	}
	else
	{
		perror("Send error");
		return EXIT_FAILURE;
	}

	size = recvmsg(sock_fd, &msg, 0);
	(void)size;

	printf("Recieved: %s\n", (char *)NLMSG_DATA(nlh));

	// irq_count = syscall(COUNTER_SYSCALL);
	// printf("Counter before reset: %ld", irq_count);
	// syscall(RESET_SYSCALL);
	// irq_count = syscall(COUNTER_SYSCALL);
	// printf("Counter after reset: %ld", irq_count);

	nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
	nlh->nlmsg_pid = getpid();
	nlh->nlmsg_flags = 0;
	strcpy(NLMSG_DATA(nlh), "getdate");

	iov.iov_base = (void *)nlh;
	iov.iov_len = nlh->nlmsg_len;
	msg.msg_name = (void *)&dest_addr;
	msg.msg_namelen = sizeof(dest_addr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	res = sendmsg(sock_fd, &msg, 0);
	if(res > 0)
	{
		printf("Sent msg\n");
	}
	else
	{
		perror("Send error");
		return EXIT_FAILURE;
	}

	size = recvmsg(sock_fd, &msg, 0);
	(void)size;

	printf("Recieved: %s\n", (char *)NLMSG_DATA(nlh));

	close(sock_fd);	
}