long kbd_irq_count = 0;
ktime_t kbd_time;

EXPORT_SYMBOL(kbd_irq_count);
EXPORT_SYMBOL(kbd_time);

SYSCALL_DEFINE0(kbd_reset_counter)
{
  kbd_time = ktime_get_real();
  kbd_irq_count = 0;
  return 0;
}

SYSCALL_DEFINE0(kbd_irq_counter)
{
  return kbd_irq_count;
}