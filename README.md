# Linux kernel module that counts keyboard interrupts

Module that counts interrupts and uses sockets and syscalls to communicate with kernel

In this project there are two folders:
- kbd_module - contains kernel module for keyboard interrupts and client app to test it
- kernel_module - contains syscalls, has to be compiled alongside kernel

# Compiling

## kbd_module

Invoke `make` inside the folder. It compiles kernel module and client app. To load/unload module you can use convienience scripts `load.sh` and `unload.sh`.

To be sure everything works use `dmesg | tail` and check if there were any errors during module loading.

Keyboards can be serviced on different interrupt lines. To test your keyboard you need to find on which line your keyboard is handled (e.g check `cat /proc/interrupt`). You can set interrupt line by changing macro `KEYBOARD_IRQ_NR` inside `kbd_module.c`. By default it's set for interrupt number 1, which is default for linux. Sadly i couldn't get it to work with USB keyboard, existing drivers didn't want to share.

## kernel_module

Needs to compiled with your kernel to work. Remember about adding syscalls to system call table:
```
329 common  kbd_reset_counter sys_kbd_reset_counter
330 common  kbd_irq_counter sys_kbd_irq_counter
```

! Syscall numbers will vary between different kernels !.

Recommend copying contents of `kbd_kernel_module.c` to your `kernel/sys.c` in your Linux source code folder and follow guide, like [this](https://brennan.io/2016/11/14/kernel-dev-ep3/) for example.

# Keyboard module

When module is loaded it registers interrupt handler to listen to keyboard interrupts (and count them). Also opens netlink socket and listens for messages. When you send to it string "getdate" it will send back string representation of date when:
- interrupt counter was last reset or...
- module was loaded, if no resets were made.

You can also ask for interrupts count by message "getirqcount". It also sends back text representation of count.

To reset interrupt counter you can use syscall `kbd_reset_counter`. Theres also syscall to get number of interrupts: `kbd_irq_counter`. Syscall numbers can vary depending on system.

# Client application

Opens netlink socket to kbd_module (if it's not loaded app will throw error) and sends request for last counter reset. Then uses syscalls to read interrupt count, then resets count and asks for date once again. Hopefully it changed :).

# Closing notes

I am not expert and i don't actually recommend to try this code, it was done mostly for educational purposes :D